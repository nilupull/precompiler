console.log("Started")
var Validator = require('./app/validator');
var Tarjans = require('./app/tarjans')();



// const validator = new Validator();
// validator.validate(getJSON());


function getMockDataJSON() {
    return "{\"vertices\":[{\"id\":0,\"component_type\":\"initiate\",\"entry_edges\":[],\"exit_edges\":[0],\"is_divergent\":false,\"is_convergent\":false,\"is_disconnected\":false,\"inputs\":[{\"key\":\"firstName\",\"value\":\"koka\",\"type\":\"string\"},{\"key\":\"lastName\",\"value\":\"siva\",\"type\":\"string\"}],\"outputs\":[],\"paths_from_root\":[]},{\"id\":1,\"component_type\":\"logic\",\"entry_edges\":[0],\"exit_edges\":[1,2],\"is_divergent\":true,\"logicType\":\"or\",\"is_convergent\":false,\"is_disconnected\":false,\"inputs\":[{\"key\":\"firstName\",\"value\":\"ashan\",\"type\":\"string\"},{\"key\":\"lastName\",\"value\":\"kumara\",\"type\":\"string\"}],\"outputs\":[],\"guranteed_inputs\":[],\"paths_from_root\":[[0]]},{\"id\":2,\"component_type\":\"input\",\"entry_edges\":[1],\"exit_edges\":[3],\"is_divergent\":false,\"is_convergent\":false,\"is_disconnected\":false,\"inputs\":[{\"key\":\"husni\",\"value\":\"ahmad\",\"type\":\"string\"},{\"key\":\"lastName\",\"value\":\"siva\",\"type\":\"string\"}],\"outputs\":[],\"guranteed_inputs\":[],\"paths_from_root\":[[0,1]]},{\"id\":3,\"component_type\":\"input\",\"entry_edges\":[2],\"exit_edges\":[4],\"is_divergent\":false,\"is_convergent\":false,\"is_disconnected\":false,\"inputs\":[],\"outputs\":[],\"guranteed_inputs\":[],\"paths_from_root\":[[0,2]]},{\"id\":4,\"component_type\":\"logic\",\"entry_edges\":[3,4],\"exit_edges\":[5],\"is_divergent\":false,\"logicType\":\"and\",\"is_convergent\":true,\"is_disconnected\":false,\"inputs\":[],\"outputs\":[],\"guranteed_inputs\":[],\"paths_from_root\":[[0,1,3],[0,2,4]]},{\"id\":5,\"component_type\":\"input\",\"entry_edges\":[5,6],\"exit_edges\":[],\"is_divergent\":false,\"is_convergent\":false,\"is_disconnected\":false,\"inputs\":[],\"outputs\":[],\"guranteed_inputs\":[],\"paths_from_root\":[[0,1,3,5],[0,2,4,5]]},{\"id\":6,\"component_type\":\"input\",\"entry_edges\":[],\"exit_edges\":[6],\"is_divergent\":false,\"is_convergent\":false,\"is_disconnected\":false,\"inputs\":[],\"outputs\":[],\"guranteed_inputs\":[],\"paths_from_root\":[]}],\"edges\":[{\"id\":0,\"start_vertex_id\":0,\"end_vertex_id\":1,\"weight\":1},{\"id\":1,\"start_vertex_id\":1,\"end_vertex_id\":2,\"weight\":1},{\"id\":2,\"start_vertex_id\":1,\"end_vertex_id\":3,\"weight\":1},{\"id\":3,\"start_vertex_id\":2,\"end_vertex_id\":4,\"weight\":1},{\"id\":4,\"start_vertex_id\":3,\"end_vertex_id\":4,\"weight\":1},{\"id\":5,\"start_vertex_id\":4,\"end_vertex_id\":5,\"weight\":1},{\"id\":6,\"start_vertex_id\":6,\"end_vertex_id\":5,\"weight\":1}]}";

}

function getMockDataJSONLoop() {
    return "{\r\n  \"vertices\": [\r\n    {\r\n      \"id\": 0,\r\n      \"component_type\": \"initiate\",\r\n      \"entry_edges\": [],\r\n      \"exit_edges\": [\r\n        0\r\n      ],\r\n      \"is_divergent\": false,\r\n      \"is_convergent\": false,\r\n      \"is_disconnected\": false,\r\n      \"inputs\": [\r\n        {\r\n          \"key\": \"firstName\",\r\n          \"value\": \"koka\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"lastName\",\r\n          \"value\": \"siva\",\r\n          \"type\": \"string\"\r\n        }\r\n      ],\r\n      \"outputs\": [],\r\n      \"paths_from_root\": [],\r\n      \"isDisconnected\": false,\r\n      \"guranteed_inputs\": []\r\n    },\r\n    {\r\n      \"id\": 1,\r\n      \"component_type\": \"logic\",\r\n      \"entry_edges\": [\r\n        1,\r\n        0\r\n      ],\r\n      \"exit_edges\": [\r\n        2\r\n      ],\r\n      \"is_divergent\": true,\r\n      \"logicType\": \"or\",\r\n      \"is_convergent\": false,\r\n      \"is_disconnected\": false,\r\n      \"inputs\": [\r\n        {\r\n          \"key\": \"firstName\",\r\n          \"value\": \"ashan\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"lastName\",\r\n          \"value\": \"kumara\",\r\n          \"type\": \"string\"\r\n        }\r\n      ],\r\n      \"outputs\": [],\r\n      \"guranteed_inputs\": [\r\n        {\r\n          \"key\": \"firstName\",\r\n          \"value\": \"koka\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"lastName\",\r\n          \"value\": \"siva\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"firstName\",\r\n          \"value\": \"ashan\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"lastName\",\r\n          \"value\": \"kumara\",\r\n          \"type\": \"string\"\r\n        }\r\n      ],\r\n      \"paths_from_root\": [],\r\n      \"isDisconnected\": true\r\n    },\r\n    {\r\n      \"id\": 2,\r\n      \"component_type\": \"input\",\r\n      \"entry_edges\": [\r\n        3\r\n      ],\r\n      \"exit_edges\": [\r\n        1\r\n      ],\r\n      \"is_divergent\": false,\r\n      \"is_convergent\": false,\r\n      \"is_disconnected\": false,\r\n      \"inputs\": [\r\n        {\r\n          \"key\": \"husni\",\r\n          \"value\": \"ahmad\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"lastName\",\r\n          \"value\": \"siva\",\r\n          \"type\": \"string\"\r\n        }\r\n      ],\r\n      \"outputs\": [],\r\n      \"guranteed_inputs\": [\r\n        {\r\n          \"key\": \"firstName\",\r\n          \"value\": \"koka\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"lastName\",\r\n          \"value\": \"siva\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"firstName\",\r\n          \"value\": \"ashan\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"lastName\",\r\n          \"value\": \"kumara\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"husni\",\r\n          \"value\": \"ahmad\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"lastName\",\r\n          \"value\": \"siva\",\r\n          \"type\": \"string\"\r\n        }\r\n      ],\r\n      \"paths_from_root\": [],\r\n      \"isDisconnected\": true\r\n    },\r\n    {\r\n      \"id\": 3,\r\n      \"component_type\": \"input\",\r\n      \"entry_edges\": [\r\n        2\r\n      ],\r\n      \"exit_edges\": [\r\n        4\r\n      ],\r\n      \"is_divergent\": false,\r\n      \"is_convergent\": false,\r\n      \"is_disconnected\": false,\r\n      \"inputs\": [],\r\n      \"outputs\": [],\r\n      \"guranteed_inputs\": [\r\n        {\r\n          \"key\": \"firstName\",\r\n          \"value\": \"koka\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"lastName\",\r\n          \"value\": \"siva\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"firstName\",\r\n          \"value\": \"ashan\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"lastName\",\r\n          \"value\": \"kumara\",\r\n          \"type\": \"string\"\r\n        }\r\n      ],\r\n      \"paths_from_root\": [],\r\n      \"isDisconnected\": true\r\n    },\r\n    {\r\n      \"id\": 4,\r\n      \"component_type\": \"logic\",\r\n      \"entry_edges\": [\r\n        4\r\n      ],\r\n      \"exit_edges\": [\r\n        3,\r\n        5\r\n      ],\r\n      \"is_divergent\": false,\r\n      \"logicType\": \"and\",\r\n      \"is_convergent\": true,\r\n      \"is_disconnected\": false,\r\n      \"inputs\": [],\r\n      \"outputs\": [],\r\n      \"guranteed_inputs\": [\r\n        {\r\n          \"key\": \"firstName\",\r\n          \"value\": \"koka\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"lastName\",\r\n          \"value\": \"siva\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"firstName\",\r\n          \"value\": \"ashan\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"lastName\",\r\n          \"value\": \"kumara\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"husni\",\r\n          \"value\": \"ahmad\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"lastName\",\r\n          \"value\": \"siva\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"firstName\",\r\n          \"value\": \"koka\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"lastName\",\r\n          \"value\": \"siva\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"firstName\",\r\n          \"value\": \"ashan\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"lastName\",\r\n          \"value\": \"kumara\",\r\n          \"type\": \"string\"\r\n        }\r\n      ],\r\n      \"paths_from_root\": [],\r\n      \"invalid\": true,\r\n      \"isDisconnected\": true\r\n    },\r\n    {\r\n      \"id\": 5,\r\n      \"component_type\": \"input\",\r\n      \"entry_edges\": [\r\n        5,\r\n        6\r\n      ],\r\n      \"exit_edges\": [],\r\n      \"is_divergent\": false,\r\n      \"is_convergent\": true,\r\n      \"is_disconnected\": false,\r\n      \"inputs\": [],\r\n      \"outputs\": [],\r\n      \"guranteed_inputs\": [\r\n        {\r\n          \"key\": \"firstName\",\r\n          \"value\": \"koka\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"lastName\",\r\n          \"value\": \"siva\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"firstName\",\r\n          \"value\": \"ashan\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"lastName\",\r\n          \"value\": \"kumara\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"husni\",\r\n          \"value\": \"ahmad\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"lastName\",\r\n          \"value\": \"siva\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"firstName\",\r\n          \"value\": \"koka\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"lastName\",\r\n          \"value\": \"siva\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"firstName\",\r\n          \"value\": \"ashan\",\r\n          \"type\": \"string\"\r\n        },\r\n        {\r\n          \"key\": \"lastName\",\r\n          \"value\": \"kumara\",\r\n          \"type\": \"string\"\r\n        }\r\n      ],\r\n      \"paths_from_root\": [],\r\n      \"isDisconnected\": false\r\n    },\r\n    {\r\n      \"id\": 6,\r\n      \"component_type\": \"input\",\r\n      \"entry_edges\": [],\r\n      \"exit_edges\": [\r\n        6\r\n      ],\r\n      \"is_divergent\": false,\r\n      \"is_convergent\": false,\r\n      \"is_disconnected\": false,\r\n      \"inputs\": [],\r\n      \"outputs\": [],\r\n      \"guranteed_inputs\": [],\r\n      \"paths_from_root\": [],\r\n      \"isDisconnected\": true\r\n    }\r\n  ],\r\n  \"edges\": [\r\n    {\r\n      \"id\": 0,\r\n      \"start_vertex_id\": 0,\r\n      \"end_vertex_id\": 1,\r\n      \"weight\": 1\r\n    },\r\n    {\r\n      \"id\": 1,\r\n      \"start_vertex_id\": 2,\r\n      \"end_vertex_id\": 1,\r\n      \"weight\": 1\r\n    },\r\n    {\r\n      \"id\": 2,\r\n      \"start_vertex_id\": 1,\r\n      \"end_vertex_id\": 3,\r\n      \"weight\": 1\r\n    },\r\n    {\r\n      \"id\": 3,\r\n      \"start_vertex_id\": 4,\r\n      \"end_vertex_id\": 2,\r\n      \"weight\": 1\r\n    },\r\n    {\r\n      \"id\": 4,\r\n      \"start_vertex_id\": 3,\r\n      \"end_vertex_id\": 4,\r\n      \"weight\": 1\r\n    },\r\n    {\r\n      \"id\": 5,\r\n      \"start_vertex_id\": 4,\r\n      \"end_vertex_id\": 5,\r\n      \"weight\": 1\r\n    },\r\n    {\r\n      \"id\": 6,\r\n      \"start_vertex_id\": 6,\r\n      \"end_vertex_id\": 5,\r\n      \"weight\": 1\r\n    }\r\n  ]\r\n}";

}

function getJSON(){
    return JSON.stringify(getOBJ());

}
function getOBJ(){
    var graph= {
        "vertices": [
            {
                "id": 0,
                "component_type": "initiate",
                "entry_edges": [],
                "exit_edges": [
                    0
                ],
                "is_divergent": false,
                "is_convergent": false,
                "is_disconnected": false,
                "inputs": [
                    {
                        "key": "firstName",
                        "value": "koka",
                        "type": "string"
                    },
                    {
                        "key": "lastName",
                        "value": "siva",
                        "type": "string"
                    }
                ],
                "outputs": [],
                "paths_from_root": [],
                "isDisconnected": false,
                "guranteed_inputs": []
            },
            {
                "id": 1,
                "component_type": "logic",
                "entry_edges": [
                    1,
                    0
                ],
                "exit_edges": [
                    2
                ],
                "is_divergent": true,
                "logicType": "or",
                "is_convergent": false,
                "is_disconnected": false,
                "inputs": [
                    {
                        "key": "firstName",
                        "value": "ashan",
                        "type": "string"
                    },
                    {
                        "key": "lastName",
                        "value": "kumara",
                        "type": "string"
                    }
                ],
                "outputs": [],
                "guranteed_inputs": [
                    {
                        "key": "firstName",
                        "value": "koka",
                        "type": "string"
                    },
                    {
                        "key": "lastName",
                        "value": "siva",
                        "type": "string"
                    },
                    {
                        "key": "firstName",
                        "value": "ashan",
                        "type": "string"
                    },
                    {
                        "key": "lastName",
                        "value": "kumara",
                        "type": "string"
                    }
                ],
                "paths_from_root": [],
                "isDisconnected": true
            },
            {
                "id": 2,
                "component_type": "input",
                "entry_edges": [
                    3
                ],
                "exit_edges": [
                    1
                ],
                "is_divergent": false,
                "is_convergent": false,
                "is_disconnected": false,
                "inputs": [
                    {
                        "key": "husni",
                        "value": "ahmad",
                        "type": "string"
                    },
                    {
                        "key": "lastName",
                        "value": "siva",
                        "type": "string"
                    }
                ],
                "outputs": [],
                "guranteed_inputs": [
                    {
                        "key": "firstName",
                        "value": "koka",
                        "type": "string"
                    },
                    {
                        "key": "lastName",
                        "value": "siva",
                        "type": "string"
                    },
                    {
                        "key": "firstName",
                        "value": "ashan",
                        "type": "string"
                    },
                    {
                        "key": "lastName",
                        "value": "kumara",
                        "type": "string"
                    },
                    {
                        "key": "husni",
                        "value": "ahmad",
                        "type": "string"
                    },
                    {
                        "key": "lastName",
                        "value": "siva",
                        "type": "string"
                    }
                ],
                "paths_from_root": [],
                "isDisconnected": true
            },
            {
                "id": 3,
                "component_type": "input",
                "entry_edges": [
                    2
                ],
                "exit_edges": [
                    4
                ],
                "is_divergent": false,
                "is_convergent": false,
                "is_disconnected": false,
                "inputs": [],
                "outputs": [],
                "guranteed_inputs": [
                    {
                        "key": "firstName",
                        "value": "koka",
                        "type": "string"
                    },
                    {
                        "key": "lastName",
                        "value": "siva",
                        "type": "string"
                    },
                    {
                        "key": "firstName",
                        "value": "ashan",
                        "type": "string"
                    },
                    {
                        "key": "lastName",
                        "value": "kumara",
                        "type": "string"
                    }
                ],
                "paths_from_root": [],
                "isDisconnected": true
            },
            {
                "id": 4,
                "component_type": "logic",
                "entry_edges": [
                    4
                ],
                "exit_edges": [
                    3,
                    5
                ],
                "is_divergent": false,
                "logicType": "and",
                "is_convergent": true,
                "is_disconnected": false,
                "inputs": [],
                "outputs": [],
                "guranteed_inputs": [
                    {
                        "key": "firstName",
                        "value": "koka",
                        "type": "string"
                    },
                    {
                        "key": "lastName",
                        "value": "siva",
                        "type": "string"
                    },
                    {
                        "key": "firstName",
                        "value": "ashan",
                        "type": "string"
                    },
                    {
                        "key": "lastName",
                        "value": "kumara",
                        "type": "string"
                    },
                    {
                        "key": "husni",
                        "value": "ahmad",
                        "type": "string"
                    },
                    {
                        "key": "lastName",
                        "value": "siva",
                        "type": "string"
                    },
                    {
                        "key": "firstName",
                        "value": "koka",
                        "type": "string"
                    },
                    {
                        "key": "lastName",
                        "value": "siva",
                        "type": "string"
                    },
                    {
                        "key": "firstName",
                        "value": "ashan",
                        "type": "string"
                    },
                    {
                        "key": "lastName",
                        "value": "kumara",
                        "type": "string"
                    }
                ],
                "paths_from_root": [],
                "invalid": true,
                "isDisconnected": true
            },
            {
                "id": 5,
                "component_type": "input",
                "entry_edges": [
                    5,
                    6
                ],
                "exit_edges": [],
                "is_divergent": false,
                "is_convergent": true,
                "is_disconnected": false,
                "inputs": [],
                "outputs": [],
                "guranteed_inputs": [
                    {
                        "key": "firstName",
                        "value": "koka",
                        "type": "string"
                    },
                    {
                        "key": "lastName",
                        "value": "siva",
                        "type": "string"
                    },
                    {
                        "key": "firstName",
                        "value": "ashan",
                        "type": "string"
                    },
                    {
                        "key": "lastName",
                        "value": "kumara",
                        "type": "string"
                    },
                    {
                        "key": "husni",
                        "value": "ahmad",
                        "type": "string"
                    },
                    {
                        "key": "lastName",
                        "value": "siva",
                        "type": "string"
                    },
                    {
                        "key": "firstName",
                        "value": "koka",
                        "type": "string"
                    },
                    {
                        "key": "lastName",
                        "value": "siva",
                        "type": "string"
                    },
                    {
                        "key": "firstName",
                        "value": "ashan",
                        "type": "string"
                    },
                    {
                        "key": "lastName",
                        "value": "kumara",
                        "type": "string"
                    }
                ],
                "paths_from_root": [],
                "isDisconnected": false
            }
        ],
        "edges": [
            {
                "id": 0,
                "start_vertex_id": 0,
                "end_vertex_id": 1,
                "weight": 1
            },
            {
                "id": 1,
                "start_vertex_id": 2,
                "end_vertex_id": 1,
                "weight": 1
            },
            {
                "id": 2,
                "start_vertex_id": 1,
                "end_vertex_id": 3,
                "weight": 1
            },
            {
                "id": 3,
                "start_vertex_id": 4,
                "end_vertex_id": 2,
                "weight": 1
            },
            {
                "id": 4,
                "start_vertex_id": 3,
                "end_vertex_id": 4,
                "weight": 1
            },
            {
                "id": 5,
                "start_vertex_id": 4,
                "end_vertex_id": 5,
                "weight": 1
            }
        ]
    }
    return graph;
}