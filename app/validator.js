/**
 * Created by Nilupul Liyanage on 11/16/2016.
 */

module.exports = class Validator {
    constructor() {
    }

    validate(graphJSON) {
        var graph = JSON.parse(graphJSON);
        console.log(JSON.stringify(graph));
        this.validateLogicalErrors(graph);
        this.validateStructuralErrors(graph);
        this.validateGuaranteedData(graph);
        console.log(this.findConnectedComponents(graph));
        console.log(JSON.stringify(graph));
    }

    validateGuaranteedData(graph) {
        this.getGuaranteedDataForLinearFlow(graph);
    }

    getGuaranteedDataForLinearFlow(graph) {
        for (var i = 0; i < graph.vertices.length; i++) {
            var guaranteedFields = [];
            for (var k = 0; k < graph.vertices[i].paths_from_root.length; k++) {
                var path = graph.vertices[i].paths_from_root[k];
                for (var j = 0; j < path.length; j++) {

                    guaranteedFields = guaranteedFields.concat(graph.vertices[path[j]].inputs);
                    guaranteedFields = guaranteedFields.concat(graph.vertices[path[j]].outputs);
                }
            }
            graph.vertices[i].guranteed_inputs = guaranteedFields;
        }
    }

    validateStructuralErrors(graph) {
        const adjacencyMatrix = this.getAdjacencyMatrix(graph);
        this.updatePossiblePaths(graph, adjacencyMatrix);
        this.initConDiv(graph);
        this.updateConnectedNodes(graph, this.findInitiateNodeIndex(graph.vertices));
    }

    updatePossiblePaths(graph, adjacencyMatrix) {
        for (var i = 0; i < graph.vertices.length; i++) {
            graph.vertices[i].paths_from_root = this.getPossiblePaths(adjacencyMatrix, graph.vertices[i].id + '', '0');
        }
    }

    getAdjacencyMatrix(graph) {
        var vertices = graph.vertices;
        var edges = graph.edges;
        var adjacencyMatrix = [];
        for (var i = 0; i < vertices.length; i++) {
            for (var j = 0; j < vertices[i].exit_edges.length; j++) {
                adjacencyMatrix.push([i + "", edges[vertices[i].exit_edges[j]].end_vertex_id + ""]);
            }
        }
        return adjacencyMatrix;
    }

    getPossiblePaths(graph, from, to) {
        return paths({graph, from: to, to: from});
        function paths({graph = [], from, to}, path = []) {
            const linkedNodes = memoize(nodes.bind(null, graph));
            return explore(from, to);

            function explore(currNode, to, paths = []) {
                path.push(currNode);
                for (let linkedNode of linkedNodes(currNode)) {
                    if (linkedNode === to) {
                        let result = path.slice(); // copy values
                        result.push(to);
                        paths.push(result);
                        continue;
                    }
                    // do not re-explore edges
                    if (!hasEdgeBeenFollowedInPath({
                            edge: {
                                from: currNode,
                                to: linkedNode
                            },
                            path
                        })) {
                        explore(linkedNode, to, paths);
                    }
                }
                path.pop(); // sub-graph fully explored
                return paths;
            }
        }

        /**
         * Get all nodes linked
         * to from `node`.
         */
        function nodes(graph, node) {
            return graph.reduce((p, c) => {
                (c[0] === node) && p.push(c[1]);
            return p;
        }, []);
        }

        /**
         * Has an edge been followed
         * in the given path?
         */
        function hasEdgeBeenFollowedInPath({edge, path}) {
            var indices = allIndices(path, edge.from);
            return indices.some(i => path[i + 1] === edge.to);
        }

        /**
         * Utility to get all indices of
         * values matching `val` in `arr`.
         */
        function allIndices(arr, val) {
            var indices = [],
                i;
            for (i = 0; i < arr.length; i++) {
                if (arr[i] === val) {
                    indices.push(i);
                }
            }
            return indices;
        }

        /**
         * Avoids recalculating linked
         * nodes.
         */
        function memoize(fn) {
            const cache = new Map();
            return function () {
                var key = JSON.stringify(arguments);
                var cached = cache.get(key);
                if (cached) {
                    return cached;
                }
                cached = fn.apply(this, arguments)
                cache.set(key, cached);
                return cached;
            };
        }
    }


    initConDiv(graph) {
        for (var i = 0; i < graph.vertices.length; i++) {
            this.isConvergentInit(graph.vertices[i]);
            this.isDivergentInit(graph.vertices[i]);
        }
        return graph;
    }

    isConvergentInit(vertex) {
        if (vertex.entry_edges.length > 1) {
            vertex.is_convergent = true;
        }
    }

    isDivergentInit(vertex) {

        if (vertex.exit_edges.length > 1) {
            vertex.is_divergent = true;
        }
    }

    findInitiateNodeIndex(vertices) {
        for (var key in vertices) {
            if (vertices[key].component_type == "initiate") {
                return key;
            }
        }
    }

    findConnectedComponents(graph) {
        var vertices = graph.vertices;
        var edges = graph.edges;
        var count = 1;
        var bfsInfo = this.updateConnectedNodes(graph, 0);
        for (var i = 0; i < vertices.length; i++) {
            if (!bfsInfo[0].discovered) {
                bfsInfo[0].discovered = true;
            }
            else if (!bfsInfo[i].discovered) {
                count++;
                bfsInfo = this.updateConnectedNodes(graph, i);
            }
        }
        return count;
    };

    findShortestPath(start, end, bfsInfo) {
        if (start === end || end === null) {
            console.log(start);
        } else {
            this.findShortestPath(start, bfsInfo[end].predecessor, bfsInfo);
            console.log(end);
        }
    };

    /*
     * Performs a breadth-first search on a graph
     * @param {array} graph - Graph, represented as adjacency lists.
     * @param {number} source - The index of the source vertex.
     * @returns {array} Array of objects describing each vertex, like
     *     [{distance: _, predecessor: _ }]
     */
    updateConnectedNodes(graph, source) {
        var bfsInfo = [];
        var edges = graph.edges;

        for (var i = 0; i < graph.vertices.length; i++) {
            if (i === 0) {
                graph.vertices[i].isDisconnected = false;
            } else {
                graph.vertices[i].isDisconnected = true;
            }
            bfsInfo[i] = {
                distance: null,
                predecessor: null,
                processed: false,
                discovered: false
            };
        }
        bfsInfo[source].distance = 0;

        var queue = [];
        queue.push(source);

        // Traverse the graph

        // As long as the queue is not empty:
        //  Repeatedly dequeue a vertex u from the queue.
        //
        //  For each neighbor v of u that has not been visited:
        //     Set distance to 1 greater than u's distance
        //     Set predecessor to u
        //     Enqueue v
        //
        var u, v, adjList;
        while (queue.length > 0) {
            u = queue.shift();
            bfsInfo[u].processed = true;
            adjList = graph.vertices[u].exit_edges;
            for (var j = 0; j < adjList.length; j++) {
                v = edges[adjList[j]].end_vertex_id;
                /* A vertex is considered processed after we have traversed
                 * all outgoing edges from it. */
                if (!bfsInfo[v].processed) {
                    //TODO check this condition
                }
                if (!bfsInfo[v].discovered) {
                    graph.vertices[v].isDisconnected = false;
                    bfsInfo[v].discovered = true;
                    bfsInfo[v].predecessor = u;
                    bfsInfo[v].distance = bfsInfo[u].distance + 1;
                    queue.push(v);
                }
            }
        }
        return bfsInfo;
    };

    findDivergents(vertices) {

        var divergentNodes = [];
        for (var i = 0; i < vertices.length; i++) {
            if (vertices[i].is_divergent) {
                divergentNodes.push(vertices[i]);
            }
        }
        return divergentNodes;
    }

    validateLogicalErrors(graph) {
        //var graph = JSON.parse(graphJSON);
        var convergentNodes = this.findConvergents(graph.vertices);

        for (var i = convergentNodes.length - 1; i >= 0; i--) {
            for (var j = 0; j < graph.vertices[convergentNodes[i].id].paths_from_root.length; j++) {
                for (var x = 0; x < graph.vertices[convergentNodes[i].id].paths_from_root[j].length; x++) {
                    if (graph.vertices[graph.vertices[convergentNodes[i].id].paths_from_root[j][x]].logicType == 'or' && convergentNodes[i].logicType == 'and') {
                        graph.vertices[convergentNodes[i].id].invalid = true;
                    }
                }
            }
        }
        return graph;
    }

    findConvergents(vertices) {

        var convergentNodes = [];
        for (var i = 0; i < vertices.length; i++) {
            if (vertices[i].is_convergent) {
                convergentNodes.push(vertices[i]);
            }
        }
        return convergentNodes;
    }

};